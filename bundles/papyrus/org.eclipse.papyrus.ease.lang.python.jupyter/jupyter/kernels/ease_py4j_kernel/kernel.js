/*****************************************************************************
 * Copyright (c) 2019 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/

function shutdownEASE() {
	var code = 'env= loadModule("Environment")\n'
			+ 'env.getScriptEngine().terminate()\n'
	var kernel = IPython.notebook.kernel;
	kernel.execute(code);
}

window.addEventListener('unload', shutdownEASE);

window.onbeforeunload = shutdownEASE;

code_show=true;

var handler = function() {
	if (code_show) {
		$('div.input').hide();
	} else {
		$('div.input').show();
	}
	code_show = !code_show

};

var action = {
	icon : 'fa-code', // a font-awesome class used on buttons, etc
	help : 'Toggle code',
	help_index : 'zz',
	handler : handler
};
var prefix = 'my_extension';
var action_name = 'hide-code';

var full_action_name = Jupyter.actions.register(action, action_name, prefix); 
Jupyter.toolbar.add_buttons_group([ full_action_name ]);



Jupyter.notebook.events.on('kernel_ready.Kernel', () => {
	IPython.notebook.kernel.execute(
		    "ease_init.get_cells_to_run()", 
		{
		    	iopub: {
		    		output: function(response) {
		    			var cells =   eval(response.content.data["text/plain"])  
		    			Jupyter.notebook.execute_cells(cells)
		    		}
		    	}
		},
		{
		silent: false, 
		store_history: false, 
		stop_on_error: true
		}
	)
});
