/*****************************************************************************
 * Copyright (c) 2019 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/
package org.eclipse.papyrus.ease.module;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collection;
import java.util.NoSuchElementException;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PrecisionPoint;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.ease.modules.AbstractScriptModule;
import org.eclipse.ease.modules.ScriptParameter;
import org.eclipse.ease.modules.WrapToScript;
import org.eclipse.ease.tools.ResourceTools;
import org.eclipse.ease.tools.RunnableWithResult;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.RequestConstants;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gef.ui.parts.GraphicalEditor;
import org.eclipse.gmf.runtime.diagram.core.edithelpers.CreateElementRequestAdapter;
import org.eclipse.gmf.runtime.diagram.ui.commands.ICommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.commands.SetBoundsCommand;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateConnectionViewAndElementRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateConnectionViewAndElementRequest.ConnectionViewAndElementDescriptor;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewAndElementRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewAndElementRequest.ViewAndElementDescriptor;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest.ViewDescriptor;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.emf.type.core.ClientContextManager;
import org.eclipse.gmf.runtime.emf.type.core.IClientContext;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.type.core.IHintedType;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.notation.Bounds;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.LayoutConstraint;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.papyrus.editor.PapyrusMultiDiagramEditor;
import org.eclipse.papyrus.infra.architecture.ArchitectureDescriptionUtils;
import org.eclipse.papyrus.infra.core.resource.ModelSet;
import org.eclipse.papyrus.infra.core.resource.NotFoundException;
import org.eclipse.papyrus.infra.core.services.ServiceException;
import org.eclipse.papyrus.infra.core.services.ServiceMultiException;
import org.eclipse.papyrus.infra.core.services.ServiceNotFoundException;
import org.eclipse.papyrus.infra.core.services.ServicesRegistry;
import org.eclipse.papyrus.infra.gmfdiag.common.commands.CreateViewCommand;
import org.eclipse.papyrus.infra.gmfdiag.common.service.palette.AspectUnspecifiedTypeConnectionTool.PapyrusCreateViewRequestFactory;
import org.eclipse.papyrus.infra.gmfdiag.common.utils.DiagramUtils;
import org.eclipse.papyrus.infra.types.core.utils.ElementTypeRegistryUtils;
import org.eclipse.papyrus.infra.viewpoints.policy.PolicyChecker;
import org.eclipse.papyrus.infra.viewpoints.policy.ViewPrototype;
import org.eclipse.papyrus.uml.tools.model.UmlModel;
import org.eclipse.papyrus.uml.tools.model.UmlUtils;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Package;

public class PapyrusUtilsModule extends AbstractScriptModule {
	
	

	private static IFile getDIFile(IFile inputFile) {
		IFile result = null;
		if ("uml".equals(inputFile.getFileExtension())) {
			result = (IFile) inputFile.getParent().findMember(inputFile.getName().replaceAll(".uml$", ".di"));
		} else if ("di".equals(inputFile.getFileExtension()) && inputFile.exists()) {
			result = inputFile;
		}
		return result;
	}

	/**
	 * Access to the root element of an opened Papyrus model in order to modify it
	 * within it's current transactionnal domain, allowing undo/redo
	 * 
	 * @param modelPath : path the model to access. The model has to be in the
	 *                  workspace and opened. The path follows EASE path/URI
	 *                  conventions: "workspace://" as well as EMF URIs
	 * @return
	 * @throws org.eclipse.papyrus.infra.core.services.ServiceException
	 *
	 */
	@WrapToScript
	public static  org.eclipse.uml2.uml.Package getPapyrusModel(String modelPath)
			throws RuntimeException, CoreException, URISyntaxException, IOException, ServiceException {

		Object resolved = ResourceTools.resolve(modelPath);

		
		IFile diFile = null;
		URI modelURI = null;

		if (resolved instanceof IFile) {
			diFile = getDIFile((IFile) resolved);

		} else {
			URI emfURI = null;
			if (resolved instanceof URL || resolved instanceof java.net.URI) {
				emfURI = URI.createURI(modelPath);
			}
			if (emfURI != null && emfURI.isPlatformResource()) {
				IResource referencedIFile = ResourcesPlugin.getWorkspace().getRoot()
						.findMember(emfURI.toPlatformString(true));
				if ((referencedIFile instanceof IFile)) {
					diFile = getDIFile((IFile) referencedIFile);
				}
			}

		}

		if (diFile != null) {

			IWorkbench workbench = PlatformUI.getWorkbench();
			FileEditorInput editorInput = new FileEditorInput(diFile);
			IEditorPart editorPart = null;
			winloop: for (IWorkbenchWindow win : workbench.getWorkbenchWindows()) {
				for (IWorkbenchPage page : win.getPages()) {
					editorPart = page.findEditor(editorInput);
					if (editorPart != null) {
						final IEditorPart papyPart = editorPart;
						Display.getDefault().syncExec(new Runnable() {

							@Override
							public void run() {
								page.activate(papyPart);

							}
						});

						break winloop;
					}
				}

			}

			ModelSet currentModelSet = null;
			if (editorPart != null) {
				ServicesRegistry servicesRegistry = editorPart.getAdapter(ServicesRegistry.class);
				if (servicesRegistry != null) {
					currentModelSet = servicesRegistry.getService(ModelSet.class);
				}
			}

			if (currentModelSet != null) {

				modelURI = URI.createPlatformResourceURI(diFile.getFullPath().toString(), true).trimFileExtension()
						.appendFileExtension("uml");
				Resource res = currentModelSet.getResource(modelURI, true);
				if (res != null && !res.getContents().isEmpty()) {
					EObject rootObject = res.getContents().get(0);
					if (rootObject instanceof org.eclipse.uml2.uml.Package) {
						return (Package) rootObject;
					}
				}
			}

		}

		throw new RuntimeException("Failed to find an opened Papyrus session for file: " + modelPath);
	}

	/**
	 * Gets a the first Named elements found in a Papyrus ModelSet. The named
	 * element can be in an imported model or profile.
	 * 
	 * @param context       : the context can be a String with a path following EASE
	 *                      path/URI conventions: "workspace://" as well as EMF
	 *                      URIs, pointing either to an UML or DI file. It can also
	 *                      be a model element of an already opened model.
	 * @param qualifiedName : the full qualified name of the named element to find.
	 * @return the first object found with the given qualified name.
	 */
	@WrapToScript
	public static NamedElement getPapyrusNamedElement(Object context, String qualifiedName)
			throws RuntimeException, CoreException, URISyntaxException, IOException, ServiceException {
		ResourceSet resSet = null;
		if (context instanceof String) {
			org.eclipse.uml2.uml.Package pack = getPapyrusModel((String) context);
			if (pack != null) {
				resSet = pack.eResource().getResourceSet();
			}
		} else if (context instanceof EObject && ((EObject) context).eResource() != null) {
			resSet = ((EObject) context).eResource().getResourceSet();
		}
		if (resSet != null) {
			Collection<NamedElement> elems = org.eclipse.uml2.uml.util.UMLUtil.findNamedElements(resSet, qualifiedName);
			if (!elems.isEmpty()) {
				return elems.iterator().next();
			} else {
				throw new RuntimeException("Failed to find a NamedElement with qualified name: " + qualifiedName);
			}
		} else {
			throw new RuntimeException("Failed to find a contextual resource for object " + context.toString()
					+ " when searching Named Element " + qualifiedName);
		}

	}


	
	
	
	
	@WrapToScript
	public static void createDiagram(EObject owner, String diagramArchitectureLabel, String diagramName) {
		// Collection<ViewPrototype> protos = PolicyChecker.getFor(owner).getPrototypesFor(owner);
		
		try {
			ViewPrototype proto = PolicyChecker.getFor(owner).getPrototypesFor(owner).stream().
					filter(prototype -> 
					diagramArchitectureLabel.equals(
							prototype.getLabel()))
					.findAny().get();
			
			if (proto != null) {
				boolean instanciateOk =proto.instantiateOn(owner, diagramName);
				if (instanciateOk ) {
				
				}
			}
			
					
		}catch (NoSuchElementException e) {
			throw new RuntimeException("Failed to find a diagram kind named "+diagramArchitectureLabel);
		}
		
	}

	@WrapToScript
	public static EObject createSemanticElement(EObject owner, String elementTypeIdToCreate) {

		PapyrusMultiDiagramEditor papyrus = getActivePapyrusEditor();
		IElementType elementType = getElementType(papyrus, elementTypeIdToCreate);
		if (elementType != null) {
			return RequestUtils.createElementWithRequest(owner, elementType);
		}
		return null;
	}

	@WrapToScript
	public static ViewAndElementHandler createSemanticElementAndLinkView(String graphicalElementTypeIdToCreate,
			Node sourceNode, Double relativeXSource, Double relativeYSource, Node targetNode, Double relativeXTarget,
			Double relativeYTarget) {

		PapyrusMultiDiagramEditor papyrus = getActivePapyrusEditor();
		IElementType elementType = getElementType(papyrus, graphicalElementTypeIdToCreate);

		GraphicalEditPart diagramEditPart = (GraphicalEditPart) getActivePapyrusViewer(papyrus).getContents();

		GraphicalEditPart sourceEditPart = (GraphicalEditPart) getActivePapyrusViewer(papyrus).getEditPartRegistry()
				.get(sourceNode);
		GraphicalEditPart targetEditPart = (GraphicalEditPart) getActivePapyrusViewer(papyrus).getEditPartRegistry()
				.get(targetNode);
		CreateConnectionViewAndElementRequest request = (CreateConnectionViewAndElementRequest) PapyrusCreateViewRequestFactory
				.getCreateConnectionRequest(elementType, diagramEditPart.getDiagramPreferencesHint());

		request.setSourceEditPart(sourceEditPart);

		request.setType(RequestConstants.REQ_CONNECTION_START);
		// Point sourcePoint = getRelativePoint(sourceNode, relativeXSource,
		// relativeYSource);

		Point sourcePoint = getAbsolutePoint(sourceNode, relativeXSource, relativeYSource);
		sourceEditPart.getFigure().translateToAbsolute(sourcePoint);
		request.setLocation(sourcePoint);

		Command command = sourceEditPart.getTargetEditPart(request).getCommand(request);

		if (command instanceof CompoundCommand) {
			for (Object subCommand : ((CompoundCommand) command).getCommands()) {
				if (subCommand instanceof ICommandProxy) {
					request.setStartCommand((ICommandProxy) subCommand);
					break;
				}
			}
		}

		request.setType(RequestConstants.REQ_CONNECTION_END);

		request.setTargetEditPart(targetEditPart);
		// Point targetPoint = getRelativePoint(targetNode, relativeXTarget,
		// relativeYTarget);
		Point targetPoint = getAbsolutePoint(targetNode, relativeXTarget, relativeYTarget);

		targetEditPart.getFigure().translateToAbsolute(targetPoint);
		request.setLocation(targetPoint);

		command = targetEditPart.getTargetEditPart(request).getCommand(request);

		if (command != null && command.canExecute()) {

			command.execute();
		}

		ConnectionViewAndElementDescriptor descriptor = request.getConnectionViewAndElementDescriptor();
		View view = (View) descriptor.getAdapter(View.class);
		EObject semanticElement = descriptor.getElementAdapter().getAdapter(EObject.class);

		return new ViewAndElementHandler(view, semanticElement);

	}

	@WrapToScript
	public static ViewAndElementHandler createSemanticElementAndView(String graphicalElementTypeIdToCreate, Integer x,
			Integer y, @ScriptParameter(defaultValue = ScriptParameter.NULL) Integer width,
			@ScriptParameter(defaultValue = ScriptParameter.NULL) Integer height) {

		PapyrusMultiDiagramEditor papyrus = getActivePapyrusEditor();
		IElementType elementType = getElementType(papyrus, graphicalElementTypeIdToCreate);
		EditPart targetEditPart = calculateTargetEditPart(papyrus, (double) x, (double) y);

		if (elementType instanceof IHintedType && targetEditPart instanceof GraphicalEditPart) {

			ViewAndElementDescriptor viewDescriptor = new ViewAndElementDescriptor(
					new CreateElementRequestAdapter(new CreateElementRequest(elementType)), Node.class,
					((IHintedType) elementType).getSemanticHint(),
					((GraphicalEditPart) targetEditPart).getDiagramPreferencesHint());

			CreateViewAndElementRequest request = new CreateViewAndElementRequest(viewDescriptor);

			Command command = targetEditPart.getTargetEditPart(request).getCommand(request);

			if (command != null && command.canExecute()) {

				// getPapyrusSession().getEditingDomain().getCommandStack().execute(new
				// GEFtoEMFCommandWrapper(command));
				command.execute();
			}
			View view = (View) viewDescriptor.getAdapter(View.class);
			EObject semanticElement = viewDescriptor.getElementAdapter().getAdapter(EObject.class);

			updateBounds(viewDescriptor, (TransactionalEditingDomain) papyrus.getEditingDomain(), new Point(x, y),
					width, height);

			return new ViewAndElementHandler(view, semanticElement);
		}
		return null;
	}

	@WrapToScript
	public static void refreshPapyrus() {
		// Runnable refresh = ((TransactionalEditingDomain)
		// getPapyrusSession().getEditingDomain()).createPrivilegedRunnable(new
		// Runnable() {
		Runnable refresh = new Runnable() {
			@Override
			public void run() {

				getActivePapyrusViewer(getActivePapyrusEditor()).flush();

			}
		};
		Display.getDefault().syncExec(refresh);

	}

	public static void updateBounds(IAdaptable iAdaptable, TransactionalEditingDomain domain, Point location,
			Integer width, Integer height) {
		SetBoundsCommand setBoundsCommand = null;

		if (width != null && height != null) {
			Dimension dimension = new Dimension(width, height);
			Rectangle rectangle = new Rectangle(location, dimension);

			setBoundsCommand = new SetBoundsCommand(domain, "move", iAdaptable, rectangle);
		} else {
			setBoundsCommand = new SetBoundsCommand((TransactionalEditingDomain) domain, "move", iAdaptable, location);
		}

		// getPapyrusSession().getEditingDomain().getCommandStack().execute(new
		// GMFtoEMFCommandWrapper(setBoundsCommand));
		try {
			setBoundsCommand.execute(null, null);
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@WrapToScript
	public static View createView(EObject element, String graphicalElementTypeIdToCreate, Integer x, Integer y,
			@ScriptParameter(defaultValue = ScriptParameter.NULL) Integer width,
			@ScriptParameter(defaultValue = ScriptParameter.NULL) Integer height) {

		PapyrusMultiDiagramEditor papyrus = getActivePapyrusEditor();
		IElementType elementType = getElementType(papyrus, graphicalElementTypeIdToCreate);
		EditPart targetEditPart = calculateTargetEditPart(papyrus, (double) x, (double) y);

		if (elementType instanceof IHintedType && targetEditPart instanceof GraphicalEditPart) {

			ViewDescriptor descriptor = new ViewDescriptor(new EObjectAdapter(element), Node.class,
					((IHintedType) elementType).getSemanticHint(),
					((GraphicalEditPart) targetEditPart).getDiagramPreferencesHint());

			CreateViewCommand createCommand = new CreateViewCommand(
					(TransactionalEditingDomain) papyrus.getEditingDomain(), descriptor,
					((GraphicalEditPart) targetEditPart).getNotationView());

			try {
				createCommand.execute(null, null);
				IAdaptable commandResult = (IAdaptable) createCommand.getCommandResult().getReturnValue();
				View result = commandResult.getAdapter(View.class);

				updateBounds(commandResult, (TransactionalEditingDomain) papyrus.getEditingDomain(), new Point(x, y),
						width, height);
				return result;

			} catch (ExecutionException e) {
				e.printStackTrace();
			}

		}
		return null;

	}

	@WrapToScript
	public static View createRelativeView(EObject element, String graphicalElementTypeIdToCreate, Node parentNode,
			Double relativeX, Double relativeY, @ScriptParameter(defaultValue = ScriptParameter.NULL) Integer width,
			@ScriptParameter(defaultValue = ScriptParameter.NULL) Integer height) {

		PapyrusMultiDiagramEditor papyrus = getActivePapyrusEditor();
		IElementType elementType = getElementType(papyrus, graphicalElementTypeIdToCreate);

		Diagram diagram = DiagramUtils.getContainingDiagram(parentNode);
		GraphicalEditPart diagramEditPart = (GraphicalEditPart) getActivePapyrusViewer(papyrus).getEditPartRegistry()
				.get(diagram);

		if (elementType instanceof IHintedType && diagramEditPart instanceof GraphicalEditPart) {

			ViewDescriptor descriptor = new ViewDescriptor(new EObjectAdapter(element), Node.class,
					((IHintedType) elementType).getSemanticHint(),
					((GraphicalEditPart) diagramEditPart).getDiagramPreferencesHint());

			CreateViewCommand createCommand = new CreateViewCommand(
					(TransactionalEditingDomain) papyrus.getEditingDomain(), descriptor, parentNode);

			try {
				createCommand.execute(null, null);
				IAdaptable commandResult = (IAdaptable) createCommand.getCommandResult().getReturnValue();
				View result = commandResult.getAdapter(View.class);

				updateBounds(commandResult, (TransactionalEditingDomain) papyrus.getEditingDomain(),
						getRelativePoint(parentNode, relativeX, relativeY), width, height);
				return result;
			} catch (ExecutionException e) {
				e.printStackTrace();
			}

		}
		return null;

	}

	@WrapToScript
	public static ViewAndElementHandler createRelativeSemanticElementAndView(String graphicalElementTypeIdToCreate,
			Node parentNode, Double relativeX, Double relativeY,
			@ScriptParameter(defaultValue = ScriptParameter.NULL) Integer width,
			@ScriptParameter(defaultValue = ScriptParameter.NULL) Integer height) {

		PapyrusMultiDiagramEditor papyrus = getActivePapyrusEditor();
		IElementType elementType = getElementType(papyrus, graphicalElementTypeIdToCreate);

		if (elementType instanceof IHintedType && parentNode instanceof Node) {

			GraphicalEditPart targetEditPart = null;

			if ((relativeX == 0.0) || relativeY == 0.0 || relativeX == 1.0 || relativeY == 1.0) {
				targetEditPart = (GraphicalEditPart) getActivePapyrusViewer(papyrus).getEditPartRegistry()
						.get(parentNode);
			} else {
				PrecisionPoint absoluteLocation = getAbsolutePoint(parentNode, relativeX, relativeY);
				targetEditPart = (GraphicalEditPart) calculateTargetEditPart(getActivePapyrusEditor(),
						absoluteLocation.preciseX(), absoluteLocation.preciseY());
			}

			if (targetEditPart instanceof GraphicalEditPart) {
				ViewAndElementDescriptor viewDescriptor = new ViewAndElementDescriptor(
						new CreateElementRequestAdapter(new CreateElementRequest(elementType)), Node.class,
						((IHintedType) elementType).getSemanticHint(),
						((GraphicalEditPart) targetEditPart).getDiagramPreferencesHint());

				CreateViewAndElementRequest request = new CreateViewAndElementRequest(viewDescriptor);

				Command command = targetEditPart.getTargetEditPart(request).getCommand(request);

				if (command != null && command.canExecute()) {
					command.execute();
					View view = (View) viewDescriptor.getAdapter(View.class);
					EObject semanticElement = viewDescriptor.getElementAdapter().getAdapter(EObject.class);

					updateBounds(viewDescriptor, (TransactionalEditingDomain) papyrus.getEditingDomain(),
							getRelativePoint(parentNode, relativeX, relativeY), width, height);

					return new ViewAndElementHandler(view, semanticElement);
				}

			}

		}
		return null;

	}

	private static Point getRelativePoint(Node parentNode, Double relativeX, Double relativeY) {

		LayoutConstraint constraint = parentNode.getLayoutConstraint();
		if (constraint instanceof Bounds) {

			Bounds bounds = (Bounds) constraint;
			double newX = (relativeX * bounds.getWidth());
			double newY = (relativeY * bounds.getHeight());

			return new PrecisionPoint(newX, newY);
		}
		return null;

	}

	private static PrecisionPoint getAbsolutePoint(Node parentNode, Double relativeX, Double relativeY) {

		LayoutConstraint constraint = parentNode.getLayoutConstraint();
		if (constraint instanceof Bounds) {
			Bounds bounds = (Bounds) constraint;
			Double newX = (relativeX * bounds.getWidth()) + bounds.getX();
			Double newY = (relativeY * bounds.getHeight()) + bounds.getY();

			return new PrecisionPoint(newX, newY);
		}
		return null;

	}

	public static EditPart calculateTargetEditPart(PapyrusMultiDiagramEditor papyrus, Double x, Double y) {
		PrecisionPoint location = new PrecisionPoint(x, y);

		GraphicalViewer currentViewer = getActivePapyrusViewer(papyrus);

		if (currentViewer != null) {
			GraphicalEditPart diagramPart = (GraphicalEditPart) currentViewer.getContents();

			IFigure targetFigure = diagramPart.getFigure().findFigureAt(location);
			if (targetFigure != null) {
				EditPart targetEditPart = (EditPart) currentViewer.getVisualPartMap().get(targetFigure);
				while (targetEditPart == null && targetFigure.getParent() != null) {
					targetFigure = targetFigure.getParent();
					targetEditPart = (EditPart) currentViewer.getVisualPartMap().get(targetFigure);
				}
				return targetEditPart;

			}

		}
		return null;

	}

	public static IElementType getElementType(PapyrusMultiDiagramEditor papyrus, String id) {
		ModelSet modelSet;
		try {
			modelSet = papyrus.getServicesRegistry().getService(ModelSet.class);
			ArchitectureDescriptionUtils utils = new ArchitectureDescriptionUtils(modelSet);
			IClientContext currentClientContext = ClientContextManager.getInstance()
					.getClientContext(utils.getArchitectureContextId());

			return ElementTypeRegistryUtils.getType(currentClientContext, id);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;

	}

	public static PapyrusMultiDiagramEditor getActivePapyrusEditor() {
		
		
			RunnableWithResult<IWorkbenchWindow> runnable = new RunnableWithResult<IWorkbenchWindow>() {
				@Override
			public IWorkbenchWindow runWithTry() throws Throwable {
					IWorkbenchWindow workbenchWindow = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
				return workbenchWindow;
				}
			};
			PlatformUI.getWorkbench().getDisplay().syncExec(runnable);
			IWorkbenchWindow workbenchWindow =runnable.getResult();
			IWorkbenchPage page = workbenchWindow.getActivePage();
			IEditorPart editorPart = page.getActiveEditor();
			if (editorPart != null && editorPart instanceof PapyrusMultiDiagramEditor) {
				try {
					if (!((PapyrusMultiDiagramEditor) editorPart).getServicesRegistry().isStarted(ModelSet.class.getName())){
						((PapyrusMultiDiagramEditor) editorPart).getServicesRegistry().startServicesByClassKeys(ModelSet.class);

					}
				} catch (ServiceMultiException | ServiceNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return (PapyrusMultiDiagramEditor) editorPart;
			}
		

		return null;
	}

	
	@WrapToScript
	public static ModelSet getActivePapyrusModelSet() {
		PapyrusMultiDiagramEditor papyrus = getActivePapyrusEditor();
		try {
			return papyrus.getServicesRegistry().getService(ModelSet.class);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	@WrapToScript
	public static Package getActivePapyrusModel() {
		
		ModelSet modelSet = PapyrusUtilsModule.getActivePapyrusModelSet();
		if (modelSet !=null) {
			UmlModel umlModel = UmlUtils.getUmlModel(modelSet);
			try {
				return (Package) umlModel.lookupRoot();
			} catch (NotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		return null;
	}
	
	public static GraphicalViewer getActivePapyrusViewer(PapyrusMultiDiagramEditor papyrusEditor) {

		IEditorPart currentEditor = papyrusEditor.getActiveEditor();
		if (currentEditor instanceof GraphicalEditor) {
			return (GraphicalViewer) ((GraphicalEditor) currentEditor).getAdapter(GraphicalViewer.class);
		}

		return null;
	}
	
	@WrapToScript
	public static void saveAs(String uriString) {
		String cleanedURI = uriString.replaceAll("workspace:", "platform:/resource");
		URI uri = URI.createURI(cleanedURI);
		PapyrusMultiDiagramEditor papyrus = getActivePapyrusEditor();
		try {
			ModelSet modelSet = papyrus.getServicesRegistry().getService(ModelSet.class);
			modelSet.saveAs(uri);
			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
