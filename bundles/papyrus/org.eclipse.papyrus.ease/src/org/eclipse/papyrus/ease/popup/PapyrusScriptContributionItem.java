/*****************************************************************************
 * Copyright (c) 2019 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/
package org.eclipse.papyrus.ease.popup;

import java.util.regex.Matcher;

import org.eclipse.core.expressions.Expression;
import org.eclipse.core.expressions.WithExpression;
import org.eclipse.core.internal.expressions.AdaptExpression;
import org.eclipse.core.internal.expressions.InstanceofExpression;
import org.eclipse.core.internal.expressions.IterateExpression;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.ease.ui.scripts.keywordhandler.ScriptContributionItem;
import org.eclipse.ease.ui.scripts.repository.IScript;

public class PapyrusScriptContributionItem extends ScriptContributionItem {

	public PapyrusScriptContributionItem(final IScript script) {
		super(script);
	}

	public PapyrusScriptContributionItem(final IScript script, final String enablement) {
		this(script);

		Matcher matcher = ENABLE_PATTERN.matcher(enablement);
		if (matcher.matches()) {
			try {
				WithExpression withExpression = new WithExpression("selection");
				IterateExpression iteratorExpression = new IterateExpression(null, Boolean.FALSE.toString());
				AdaptExpression adaptExpression = new AdaptExpression("org.eclipse.emf.ecore.EObject");
				InstanceofExpression instanceofExpression = new InstanceofExpression(matcher.group(1));

				withExpression.add(iteratorExpression);
				iteratorExpression.add(adaptExpression);
				adaptExpression.add(instanceofExpression);

				fVisibleExpression = withExpression;
			} catch (CoreException e) {
				// TODO provide log message to user

				fVisibleExpression = Expression.FALSE;
			}
		}
	}

}
