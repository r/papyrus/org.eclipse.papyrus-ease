
loadModule("/Modeling/PapyrusUtils")
loadModule("/Modeling/ECore")
import sys

def is_jython():
    return  hasattr(sys, 'subversion') and (sys.subversion[0]== 'Jython')

if (not is_jython()) :
    class GenericRunnable:
    
        def  __init__(self,func, *args):   
            self.func = func
            self.args = args
        
        def run(self):
            self.result= self.func(*self.args)
            
    
        class Java:
            implements=['java.lang.Runnable'] 

else:

    from java.lang import Runnable
    class GenericRunnable(Runnable):
    
        def  __init__(self,func, *args):   
            self.func = func
            self.args = args
        def run(self):
            self.result = self.func(*self.args)
    

def papyrun(func, *args):
    runnable = GenericRunnable(func, *args)
    runOperation( runnable)
    return runnable.result


    
    
