/*****************************************************************************
 * Copyright (c) 2019 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.ease.popup;

import org.eclipse.ease.ui.scripts.keywordhandler.PopupHandler;
import org.eclipse.ease.ui.scripts.keywordhandler.ScriptContributionFactory;

public class PapyrusPopupHandler extends PopupHandler {
	

	@Override
	protected String getHandlerType() {
		return "papypopup";
	}
	
	@Override
	protected ScriptContributionFactory getContributionFactory(final String location) {
		
		if (!fContributionFactories.containsKey(location))
			fContributionFactories.put(location, new PapyrusPopupScriptContributionFactory(location));

		return fContributionFactories.get(location);
	}

}
