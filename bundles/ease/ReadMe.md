Those two plugins have been tranferred from EASE Modules repository.

Their code is a copy from commit 3792a5abdf1cdfd19ddb04025c8708c5900561b9 of
repository https://git.eclipse.org/c/ease/org.eclipse.ease.modules.git

See bug https://bugs.eclipse.org/bugs/show_bug.cgi?id=579881 for more details.