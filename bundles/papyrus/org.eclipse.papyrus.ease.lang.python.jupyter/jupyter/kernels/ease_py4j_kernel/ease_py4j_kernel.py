#################################################################################
# Copyright (c) 2019 CEA LIST and others.
# 
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
#  Contributors:
#  CEA LIST - Initial API and implementation
#   
#################################################################################

code='''
import ease_init
ease_init.connect_to_ease()
'''

if __name__ == '__main__':  
    
    from ipykernel.kernelapp import IPKernelApp
    import sys
    if sys.path[0] == '':
        del sys.path[0]
    app = IPKernelApp.instance()
    app.initialize(None)
    result = app.kernel.do_execute(code,  silent=True)
    #result = app.kernel.do_execute(code2,  silent=True)
    app.start()
   
