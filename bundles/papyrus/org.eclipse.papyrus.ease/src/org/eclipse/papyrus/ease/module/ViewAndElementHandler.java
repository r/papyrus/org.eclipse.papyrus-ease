/*****************************************************************************
 * Copyright (c) 2019 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/
package org.eclipse.papyrus.ease.module;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.notation.View;

public class ViewAndElementHandler {

	private View view;
	private EObject element;
	// private GraphicalEditPart editPart;
	
	public ViewAndElementHandler(View view, EObject element) {
		this.view= view;
		this.element= element;
	}
	
	public ViewAndElementHandler() {
	
	}
	
	public void setView(View view) {
		this.view = view;
	}

	public void setElement(EObject element) {
		this.element = element;
	}

	public View getView() {
		return view;
	}
	public EObject getElement() {
		return element;
	}
	
	
}
