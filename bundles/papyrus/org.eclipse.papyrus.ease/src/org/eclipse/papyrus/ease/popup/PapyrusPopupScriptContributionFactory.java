/*****************************************************************************
 * Copyright (c) 2019 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/
package org.eclipse.papyrus.ease.popup;

import org.eclipse.ease.ui.scripts.keywordhandler.ScriptContributionFactory;
import org.eclipse.ease.ui.scripts.repository.IScript;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.ui.menus.IContributionRoot;
import org.eclipse.ui.services.IServiceLocator;

public class PapyrusPopupScriptContributionFactory extends ScriptContributionFactory {

	public PapyrusPopupScriptContributionFactory(final String location) {
		super(location);

	}

	@Override
	public void createContributionItems(final IServiceLocator serviceLocator, final IContributionRoot additions) {

		// if we added contributions manually before, do not add them a 2nd time
		// see https://bugs.eclipse.org/bugs/show_bug.cgi?id=452203 for details
		boolean rendered = (fContributionManager instanceof MenuManager) && (((MenuManager) fContributionManager).getMenu() != null);
		rendered |= (fContributionManager instanceof ToolBarManager) && (((ToolBarManager) fContributionManager).getControl() != null);

		if (rendered) {
			for (IContributionItem item : fContributionManager.getItems()) {
				if (item instanceof PapyrusScriptContributionItem) {
					// contributions already added to toolbar, do not add again
					return;
				}
			}
		}

		if (getLocation().endsWith(PapyrusPopupHandler.POPUP_LOCATION)) {
			for (IScript script : sortScripts(fScripts))
				additions.addContributionItem(new PapyrusScriptContributionItem(script, script.getKeywords().get("papypopup")), null);

		} else {
			for (IScript script : fScripts)
				additions.addContributionItem(new PapyrusScriptContributionItem(script), null);
		}
	}
}
