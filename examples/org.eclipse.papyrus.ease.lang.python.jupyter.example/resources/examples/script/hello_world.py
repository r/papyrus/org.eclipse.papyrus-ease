from ipywidgets import interact_manual

@interact_manual(message="Hello Eclipse world from a script file!")
def open_popup_with_message_from_script(message):
    ui_module.showInfoDialog(message, "Message from Jupyter")